import React from 'react';
import './Alert.css';

const Alert = props => {
    if(props.dismiss === undefined){
        return (
            <div className={['alert holder', props.type].join(' ')}
                 style={{transform: props.show ? 'translateY(0)' : 'translateY(-100vh)',
                     opacity: props.show ? '1' : '0'}}>
                {props.children}
            </div>
        );
    } else {
        return (
            <div className={['alert holder', props.type].join(' ')}
                 style={{transform: props.show ? 'translateY(0)' : 'translateY(-100vh)',
                     opacity: props.show ? '1' : '0'}}
            >
                {props.children}
                <button onClick={props.dismiss}>X</button>
            </div>
        );
    }

};

export default Alert;