import React, {useState} from 'react';
import Modal from "./components/UI/Modal/Modal";
import Alert from "./components/UI/Alert/Alert";

const App = () => {
    const[confirm, setConfirm] = useState(false);
    const[alert, setAlert] = useState(false);

    const confirmHandler = () =>{
        setConfirm(true);
    };

    const confirmCancelHandler = () =>{
        setConfirm(false);
        alertShowHandler();
    };

    const alertShowHandler = () =>{
        setAlert(true);
        setTimeout(alertDismissHandler, 4000)
    };

    const alertDismissHandler = () =>{
        setAlert(false);
    };

    return (
        <div>
            <Alert
                type='success'
                dismiss={alertDismissHandler}
                show = {alert}
            >
                This is a warning type alert
            </Alert>

            <Modal
                title="Some kinda modal title"
                show={confirm}
                cancel = {confirmCancelHandler}
            >
                <p>This is modal content</p>
            </Modal>
            <button onClick={confirmHandler}>Click</button>
        </div>
    );
};

export default App;
