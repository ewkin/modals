import React from 'react';
import './Modal.css';
import Backdrop from "../Backdrop/Backdrop";

const Modal = props => {
    return (
        <>
            <Backdrop show={props.show} onClick={props.cancel}/>
            <div
                className="Modal"
                style={{transform: props.show ? 'translateY(0)' : 'translateY(-100vh)',
                opacity: props.show ? '1' : '0'
            }}
              >
                <div className='holder'>
                <h3>{props.title}</h3>
                <button onClick={props.cancel}>X</button>
            </div>
                {props.children}
            </div>
        </>
    );
};

export default Modal;